﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Mover : NetworkBehaviour
{
    private Rigidbody2D body;
    public float MoveMultiplier = 0.3f;
    public float RotationMultiplier = 80;
    private Vector2 Acceleration = Vector2.zero;
    private float Rotation = 0.0f;
    public GameObject Bullet;
    [Range(0.1f,30f)]
    public float BulletLifeTime = 3.0f;
    [Range(0, 900f)]
    public float BulletBarrelSpeed = 180f;
    public GameObject BulletSpawnPoint;

    [SyncVar]
    public int Health = 100;

    // Start is called before the first frame update
    void Start()
    {
        body= GetComponent<Rigidbody2D>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        Rotation -= (Input.GetAxis("Horizontal") * RotationMultiplier);
        Acceleration.y = (Input.GetAxis("Vertical") * MoveMultiplier);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FireBullet();
        }
    }
    
    public void TakeDamage(int Amount)
    {
        //if (!isServer)    
    }

    void OnTriggerEnter2D(Collider2D Other) {
        Debug.Log("hit");
    }

    [Command]
    private void FireBullet()
    {
        GameObject SpawnedBullet = Instantiate(Bullet,BulletSpawnPoint.transform.position,Quaternion.identity);
        Rigidbody2D BulletBody = SpawnedBullet.GetComponent<Rigidbody2D>();
        BulletBody.rotation = body.rotation;
        BulletBody.AddRelativeForce(new Vector2(0,BulletBarrelSpeed));
        Destroy(SpawnedBullet, BulletLifeTime);
        NetworkServer.Spawn(SpawnedBullet);
    }

    void FixedUpdate()
    {
            
        body.MoveRotation(Rotation * Time.fixedDeltaTime);
        body.AddRelativeForce(Acceleration);  

    }
}
